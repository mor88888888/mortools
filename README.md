# m0rt001$

## Description
🏷 A set of multipurpose tools.

📌 The goal of this repository it isn't having a set of perfect written and ready to execute tools, it's to have the code and the use cases for its purpose collected.

![Meme](https://media.makeameme.org/created/scripts-scripts-everywhere-5c19bc.jpg)

## Installation
```bash
git clone https://gitlab.com/mor88888888/mortools.git
```

## Usage
If you want to use a specific tool, for example, written in python, here is an example:
```bash
cd mortools/others/
# If you have all the requirements installed, you can execute it. If not, check the imports reading the code. Sorry, I don't have a "requirements.txt" for every script in python.
python3 column-difference.py
# This shows you a help guide for this script. If not, read the code and see what params are required.
```

If it's written in bash, remember give it permission to execute it.
```bash
cd mortools/pentest/
chmod +x portScan.sh
./portScan.sh
# This shows you a help guide for this script. If not, read the code and see what params are required.
```

⚠ Be carefully, probably you need to modify the scripts before using it.

## Support & Contributing
I you find any bug, you have a suggestion or you don't know how works a specific tool, let me know:

🐛 [Add issue](https://gitlab.com/mor88888888/mortools/-/issues/new)

📬 mor8@protonmail.com

## Roadmap 🚧
* Ask for parameters in **all** scripts instead of editing the code to add them.
* Generate binaries to use it with the Linux terminal.

## Authors and acknowledgment
Author: @mor88888888

Thanks to:

* README.md - https://www.makeareadme.com/
* pentest/ - https://www.twitch.tv/s4vitaar
* BOF/ - https://tryhackme.com/

## Project status
🏗 I'm working in this repository actively.

🗂 It's a way to have some code I use many times collected here.

♻ I'll push new scripts or updated ones as many times as I need them in my day work.
