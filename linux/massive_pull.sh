#!/bin/bash

#projects_folder='$HOME/Documents/Workspace'
projects_folder=$1

cd $projects_folder
projects_folder=$(pwd)

ls $projects_folder > /tmp/projects.txt
projects=()

while IFS= read -r line
do
    projects+=("$line")
done < /tmp/projects.txt

# Elimino el archivo temporal
rm /tmp/projects.txt

for (( i=0; i<${#projects[@]}; i++ )); do
    echo "-- Actualizando ${projects[$i]}"
    cd $projects_folder/${projects[$i]} && git pull --quiet
done

echo "Actualizados ${#projects[@]} proyectos en $projects_folder"
