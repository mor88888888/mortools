#!/bin/bash

# Static vars
custom_conf="$HOME/.preparetohack"
platform="THM"
if [ "$SHELL" == "/bin/bash" ]; then
	echo "[INFO] Using BASH"
	rc="$HOME/.bashrc"
elif [[ "$SHELL" == "/bin/zsh" || "$SHELL" == "/usr/bin/zsh" ]]; then
	echo "[INFO] Using ZSH"
	rc="$HOME/.zshrc"
else
	echo "[ERROR] Unknown shell"
	exit 1
fi

# VPN IP
l_ip=$(ip a s tun0 2>/dev/null | grep -o -P '(?<=inet )[0-9]{1,3}(\.[0-9]{1,3}){3}')
if [ "$l_ip" == "" ]; then
	echo "[ERROR] VPN down"
	exit 1
else
	echo "IP address in the VPN: $l_ip (tun0)"
fi

# Machine name
printf "Insert the machine name: "
read machine

# Target
printf "Insert the target IP address: "
read ip

# Create the custom conf file
echo "[INFO] Creating the custom conf file in $custom_conf"
echo "export l_ip='$l_ip'" > $custom_conf
echo "export ip='$ip'" >> $custom_conf
echo "export wp='$HOME/Documents/$platform/$machine'" >> $custom_conf

# Create working directory

if [[ ! -d "$HOME/Documents/$platform/$machine" ]]; then
	mkdir "$HOME/Documents/$platform/$machine"
fi

# Call this config from rc
grep_output=$(grep "Hacking config" $rc)

if [[ $grep_output == "" ]]; then
	echo "[INFO] Calling this conf from $rc"
	echo "# Hacking config" >> $rc
	echo "source $custom_conf" >> $rc
else
	echo "[INFO] Already added conf in $rc"
fi

echo "[INFO] Reload $rc config"
exec $SHELL

echo "[INFO] Finished"
