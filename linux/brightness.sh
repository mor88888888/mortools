# -----
# XFCE Brightness script
# -----

brightness_max=$(xfpm-power-backlight-helper --get-max-brightness) # It migth be 120000
brightness_default=60000
brightness_min=20000

# Current brightness
brightness_current=$(xfpm-power-backlight-helper --get-brightness)
#echo "Current brightness $brightness_current"

if [ "$1" == "increase" ]; then
	# Increase brightness
	brightness_new=$(($brightness_current+20000))
	if (( $brightness_max < $brightness_new )); then
		echo "Full brightness"
	else
		echo "New brightness $brightness_new"
		pkexec xfpm-power-backlight-helper --set-brightness $brightness_new
	fi
elif [ "$1" == "decrease" ]; then
	# Increase brightness
        brightness_new=$(($brightness_current-20000))
        if (( $brightness_min > $brightness_new )); then
                echo "Minimum brightness"
        else
		echo "New brightness $brightness_new"
                pkexec xfpm-power-backlight-helper --set-brightness $brightness_new
        fi
else
	echo "Wrong command"
fi	
