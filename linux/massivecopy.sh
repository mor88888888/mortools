#! /bin/bash

# -----------------------------------------------------------------------------
# Find all files with a given extension and copy it to a directory
# -----------------------------------------------------------------------------
#
# -- How to use it? --
# massivecopy $extension $directory
# massivecopy mp3 ~/test

find -iregex ".*\.$1.*" -exec cp {} $2 \;
exit;
