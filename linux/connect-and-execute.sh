#! /bin/bash

# ----------------------------------------
# Auto Connect to n servers and execute a given action.
# Requirements: ssh, sshpass
# ----------------------------------------

# Requeriments
if ! command -v sshpass &> /dev/null; then
    # The package isn't installed
    echo "[ERROR] sshpass is not installed"
    exit 1
fi

declare -a servers=("test.test.com" "test2.test.com")
script='grep -r -E "ERROR" /opt > ${HOSTNAME}.log 2>/dev/null && exit'
clean_script='rm ${HOSTNAME}.log'
save_dir='/tmp/connect-and-execute.log'

printf "User: "
read user
printf "Password: "
read -s pass
echo

for server in "${servers[@]}"
do
   echo "####"
   echo "Executing script in: $server"
   sshpass -p $pass ssh -o "StrictHostKeyChecking=no" -o "LogLevel=Error" -t $user@$server $script
   echo "Copying log."
   sshpass -p $pass scp $user@$server:${server}.log $save_dir
   echo "Executing el cleanup script."
   sshpass -p $pass ssh -o "StrictHostKeyChecking=no" -o "LogLevel=Error" -t $user@$server $clean_script
   echo "####"
done

printf "Logs saved in $save_dir\n"

