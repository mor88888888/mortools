import subprocess
import pickle
import os

# Configuración de la descarga
DOWNLOAD_URL = "http://ijdxicseepozb6lfpeu3bjkhzob5mwh2wjwsiivnxxne5dt4nzhuizyd.onion/gs/GLOBALCAJA.part41.rar"  # Reemplaza con la URL real del servicio
CHUNK_SIZE = 1024  # Tamaño del fragmento para la descarga

# Configuración del archivo de estado
STATE_FILE = "estado_descarga.pickle"  # Nombre del archivo de estado

def download_file(url, file_path, start_byte=0):
	# Establecer el byte de inicio de la descarga
	headers = {"Range": f"bytes={start_byte}-"}

	# Descargar el archivo utilizando torsocks
	subprocess.run(["torsocks", "wget", "--continue", "-O", file_path, "--header", f"{headers}", url])

def save_download_state(file_path, start_byte):
	# Guardar el estado de la descarga en un archivo
	state = {"file_path": file_path, "start_byte": start_byte}
	with open(STATE_FILE, "wb") as state_file:
		pickle.dump(state, state_file)

def load_download_state():
	# Cargar el estado de la descarga desde el archivo
	try:
		with open(STATE_FILE, "rb") as state_file:
			state = pickle.load(state_file)
			return state["file_path"], state["start_byte"]
	except FileNotFoundError:
		return None, 0

def main():
	file_path, start_byte = load_download_state()

	if file_path:
		print(f"Reanudando descarga desde: {file_path} ({start_byte} bytes)")
	else:
		print("Iniciando nueva descarga")

	try:
		download_file(DOWNLOAD_URL, file_path, start_byte)
		print("Descarga completada")
		# Eliminar el archivo de estado una vez completada la descarga
		if file_path:
			os.remove(STATE_FILE)
	except KeyboardInterrupt:
		print("Descarga pausada. Guardando estado...")
		save_download_state(file_path, start_byte)

# Llamada a la función main
main()
