# mylibrary/myfunctions.py

import sys
import getopt
import requests
import json

# Vars
api_token = ''
result = []

def bulkIPInfo(api_token, ips):
    # API domain
    host='ipinfo.io'

    # Cabecera del CSV
    result.append(["IP","Hostname","CC","ORG"])

    for ip in ips:
        if ip != '':
            # Make and parse query
            url='http://'+host+'/'+ip+'?token='+api_token
            response = requests.get(url)
            data=json.loads(response.content)
            # Check vars
            try:
                country=data['country']
            except:
                country=""
            try:
                hostname=data['hostname']
            except:
                hostname=""
            try:
                org=data['org']
            except:
                org=""
            # Save result
            result.append([ip,hostname,country,org])
    
    return result

def bulkIPAbuse(api_token, ips):
    # API domain
    host='api.abuseipdb.com'

    # Cabecera del CSV
    result.append(["IP","CC","ISP","Domain","Hostnames","Score","numReports","numusersrep","lastReportedAt","whitelist"])

    for ip in ips:
        if ip != '':
            url='https://'+host+'/api/v2/check?ipAddress='+ip
            headers = {'Key': api_token}
            response = requests.get(url, headers=headers)
            data=json.loads(response.content)
            #vars
            cc=str(data['data']['countryCode'])
            isp=str(data['data']['isp'])
            domain=str(data['data']['domain'])
            hostnames=str(data['data']['hostnames'])
            score=str(data['data']['abuseConfidenceScore'])
            nrep=str(data['data']['totalReports'])
            nusers=str(data['data']['numDistinctUsers'])
            last_reported=str(data['data']['lastReportedAt'])
            whitelist=str(data['data']['isWhitelisted'])
            
            result.append([ip,cc,isp,domain,hostnames,score,nrep,nusers,last_reported,whitelist])
    
    return result

def bulkIPVoid(api_token, ips):
    # API domain
    host='endpoint.apivoid.com'

    result.append(["IP","Detections","Total Engines","Detection rate","Risk Score"])

    for ip in ips:
        if ip != '':
            url='https://'+host+'/iprep/v1/pay-as-you-go/?key='+api_token+'&ip='+ip
            response = requests.get(url)
            data=json.loads(response.content)
            
            numdetections=str(data['data']['report']['blacklists']['detections'])
            numengines=str(data['data']['report']['blacklists']['engines_count'])
            detection_rate=str(data['data']['report']['blacklists']['detection_rate'])
            risk_score=str(data['data']['report']['risk_score']['result'])
            
            result.append([ip,numdetections,numengines,detection_rate,risk_score])

            # https://docs.apivoid.com - "You need to limit requests per second to max 2-3 requests"
            time.sleep(0.5)

def flatten_json(objeto_json, parent_key='', separator='_'):
    result_parsed = {}
    for key, value in objeto_json.items():
        new_key = parent_key + separator + key if parent_key else key
        if isinstance(value, dict):
            sub_result = flatten_json(value, new_key, separator)
            result_parsed.update(sub_result)
        elif isinstance(value, list):
            for i, item in enumerate(value):
                sub_result = flatten_json(item, f"{new_key}{separator}{i}", separator)
                result_parsed.update(sub_result)
        else:
            result_parsed[new_key] = value
    return result_parsed

def bulkDomainIQS(api_token,domains):
    # API domain
    host='www.ipqualityscore.com'

    api_query='https://'+host+'/api/json/url/'+api_token+'/www.google.com'
    response = requests.get(api_query)
    data=json.loads(response.content)
    data_flattened = flatten_json(data)
    result.append(data_flattened.keys())

    # CSV content
    for domain in domains:
        if domain != '':
            api_query='https://'+host+'/api/json/url/'+api_token+'/'+domain
            response = requests.get(api_query)
            data=json.loads(response.content)
            if str(data.get('success','N/A')) == "True":
                data_flattened = flatten_json(data)
                result.append(data_flattened.values())
            else:
                result.append(str(data.get('message','N/A')))

    return result

def creditIQS(api_token):
    # API domain
    host='www.ipqualityscore.com'

    api_query='https://'+host+'/api/json/account/'+api_token
    response = requests.get(api_query)
    data=json.loads(response.content)
    
    credit=str(data['credits'])

    result.append("--> Remaining monthly queries: "+credit)
    result.append("*** Daily lookup limit of 200 API hits not included")

    return result

__all__ = ["bulkIPInfo", "bulkIPAbuse", "bulkIPVoid", "bulkDomainIQS", "creditIQS"]
