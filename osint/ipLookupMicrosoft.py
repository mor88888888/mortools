# ------------------------------------------------------------------
# Check if a given IP, subnet or a list of them belongs to any of
# the O365 services and wich one.
#
# Author: mor8@protonmail.com
#
# Usage:
# $ python3 ipLookupMicrosoft.py -f /path/to/input.txt > log.txt
# -------------------------------------------------------------------

import sys
import os
import logging
import json
from netaddr import IPNetwork, IPAddress
import argparse
from pathlib import Path

parser = argparse.ArgumentParser(description='Check if a given IPAdress belongs to a Microsoft Network.')
parser.add_argument('-ip', dest='ipaddr',
                    default="null", type=str,
                    help='Introduce a valid IP Address')
parser.add_argument('-f', dest='list', type=str,
                    default="null",
                    help='Introduce the location of a list of valids IP Adresses')

args = parser.parse_args()

if args.ipaddr != "null":
	ipToSearch = [args.ipaddr]
	print("This is the given IP Address:")
	print(ipToSearch)
	print()

elif args.list != "null":
	ips_file = open(args.list)
	ipToSearch = ips_file.read().split('\n')
	print("IPs given in the file (blanks will be ommited):")
	print(ipToSearch)
	print()

if not Path('/tmp/ipsmicrosoft.json').is_file():
	os.system("wget 'https://endpoints.office.com/endpoints/worldwide?clientrequestid=b10c5ed1-bad1-445f-b386-b919946339a7' -O /tmp/ipsmicrosoft.json")

result=[]
with open('/tmp/ipsmicrosoft.json') as json_ips:
	servicios = json.load(json_ips)
	for servicio in servicios:
		if "ips" in servicio.keys():
			for ip in ipToSearch:
				if ip != "":
					for net in servicio["ips"]:
						if ip in IPNetwork(net):
							if ip not in result:
								print("{0} --> IP: {1}".format(servicio["serviceArea"], ip))
								result.append(ip)

if not result:
	print("No match")
