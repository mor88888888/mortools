# ----------------------------------------
# Auto nslookup from a file
# ----------------------------------------

import sys, getopt
import socket

# Vars
inputfile = ''
outputfile = ''
inputfilereader = ''
outputfilereader = ''
ip_list = []
failed_list = []
last_ip = ''

# Check and save args
try:
    inputfile = sys.argv[1]
    outputfile = sys.argv[2]
except IndexError:
    print('I need 2 arguments')
    sys.exit()
 
try:
    sys.argv[3]
    print('I need 2 arguments')
    sys.exit()
except IndexError: 
    # Do nothing
    nothing='nothing'

# Read input file
inputfilereader = open(inputfile, "r")

# Read output file (create if needed)
outputfilereader = open(outputfile, "w")

lines = inputfilereader.read().split('\n')

print("###### Inicio ######")

# Cabecera del CSV
outputfilereader.write('"URL","IP"\n')

for line in lines:
    try:
        if line != '':
            ips = socket.getaddrinfo(line,0,0,0,0)
            for ip in ips:
                if last_ip != ip[-1][0]:
                    ip_list.append(ip[-1][0])
                    outputfilereader.write('"'+line+'",'+ip[-1][0]+'\n')
                last_ip=ip[-1][0]
    except socket.gaierror:
        failed_list.append(line)
        outputfilereader.write('"'+line+'","null"\n')

outputfilereader.close()

print("-- Errores:")
for failed_line in failed_list:
    print(failed_line)
print("###### Terminado ######")
