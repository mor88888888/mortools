#!/bin/bash

# ----
# Description: Script for obtaining the ubication and ASN from a IP list
# Author: mor8
# Notes: Done in bash because is faster in single task than using python-whois
# Example: ./countryLookupByWhois.sh $HOME/Downloads/proxies.txt $HOME/Downloads/output.csv
# ----

# Vars
if [[ ! $1 == '' ]]; then
    inputfile=$1
    if [[ -f $inputfile ]]; then
        echo "File given: $1"
    else
        echo "[ERROR] File given not exist"
        exit 1
    fi
else
    echo "[ERROR] No file given"
    exit 1
fi

if [[ ! $2 == '' ]]; then
    echo "Output: $2"
    outputfile=$2
    if ! touch $outputfile; then
        echo "[ERROR] Not writable directory output"
        exit 1
    fi
else
    echo "[ERROR] No output dir"
    exit 1
fi

time_per_query='0.23'
total=$(cat $inputfile | wc -l)
i=0

# Requeriments
if ! command -v whois &> /dev/null; then
    # The package isn't installed
    echo "[ERROR] whois is not installed"
    exit 1
fi

if ! command -v bc &> /dev/null; then
    # The package isn't installed
    echo "[ERROR] bc is not installed"
    exit 1
fi

# Init file (overwritting)
echo "IP;CC;ASN" > $outputfile

# Prepare terminal output:
printf "\n\n"

# Start reading the inputfile
while IFS= read -r line; do
    # Clean the input
    line=$(echo $line | sed 's/\n//')
    line=$(echo $line | sed 's/\r//')
    # Choose the records to save
    country=$(whois $line | grep -i 'country:' | head -1 | sed 's/country:        //')
    asn=$(whois $line | grep -i 'netname:' | head -1 | sed 's/netname:        //')
    # Save the line
    echo "$line;$country;$asn" >> $outputfile
    # Increment the counter
    ((i++))
    # Calculate the remaining time and percentage
    remaining=$(echo "$total-$i" | bc -l | awk '{print int($1)}')
    time_remaining=$(echo "$remaining*$time_per_query/60" | bc -l | awk '{print int($1)}')
    echo -e '\e[2A\e[KProgress:'
    printf $(echo "$i/$total*100" | bc -l | awk '{print int($1)}')
    echo "% - $i/$total - Remaining aprox $time_remaining min (guessing $time_per_query seg/query)"
done < $inputfile
