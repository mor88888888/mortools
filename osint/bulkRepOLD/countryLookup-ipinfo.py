# ------------------------------------------------------------------
# Create a CSV with the country code of a list of given IPs
# 
# It requires an API token from IPInfo.
#
# Author: mor8@protonmail.com
#
# Usage:
# $ python3 countryLookup-ipinfo.py $API_TOKEN /path/to/input.txt /path/to/output.csv
# -------------------------------------------------------------------

import sys
import getopt
import requests
import json

# Vars
inputfile = ''
outputfile = ''
inputfilereader = ''
outputfilereader = ''
ip_list = []
failed_list = []
last_ip = ''
api_token=''
host='ipinfo.io'

# Check and save args
try:
    api_token = sys.argv[1]
    inputfile = sys.argv[2]
    outputfile = sys.argv[3]
except IndexError:
    print('I need 3 arguments')
    sys.exit()

try:
    sys.argv[4]
    print('I need 3 arguments')
    sys.exit()
except IndexError:
    # Do nothing
    nothing='nothing'

# Read input file
inputfilereader = open(inputfile, "r")

# Read output file (create if needed)
outputfilereader = open(outputfile, "w")

ips = inputfilereader.read().split('\n')

print("###### Inicio ######")

# Cabecera del CSV
outputfilereader.write('"IP","Hostname","CC","ORG"\n')

for ip in ips:
    if ip != '':
        # Make and parse query
        url='http://'+host+'/'+ip+'?token='+api_token
        response = requests.get(url)
        data=json.loads(response.content)
        # Check vars
        try:
            country=data['country']
        except:
            country=""
        try:
            hostname=data['hostname']
        except:
            hostname=""
        try:
            org=data['org']
        except:
            org=""
        # Print result
        print('"'+ip+'","'+hostname+'","'+country+'","'+org+'"')
        outputfilereader.write('"'+ip+'","'+hostname+'","'+country+'","'+org+'"\n')

print("###### Terminado ######")
