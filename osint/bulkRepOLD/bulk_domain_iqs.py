# ------------------------------------------------------------------
# Checks the reputation score of a given a list of domains.
# 
# It requires an API token from www.ipqualityscore.com.
#
# Author: mor8@protonmail.com
#
# Usage:
# $ python3 bulk_domain_iqs.py $API_TOKEN /path/to/input.txt /path/to/output.csv
# -------------------------------------------------------------------

import sys
import getopt
import requests
import json
import time
import csv
from collections import OrderedDict

def flatten_json(objeto_json, parent_key='', separator='_'):
    result = {}
    for key, value in objeto_json.items():
        new_key = parent_key + separator + key if parent_key else key
        if isinstance(value, dict):
            sub_result = flatten_json(value, new_key, separator)
            result.update(sub_result)
        elif isinstance(value, list):
            for i, item in enumerate(value):
                sub_result = flatten_json(item, f"{new_key}{separator}{i}", separator)
                result.update(sub_result)
        else:
            result[new_key] = value
    return result

# Vars
inputfile = ''
outputfile = ''
inputfilereader = ''
outputfilereader = ''
api_key=''
host='www.ipqualityscore.com'

# Check and save args
try:
    api_key = sys.argv[1]
    #print(api_key)
    inputfile = sys.argv[2]
    #print(inputfile)
    outputfile = sys.argv[3]
    #print(outputfile)
except IndexError:
    print('I need 3 arguments')
    print('$ python3 bulk_domain_iqs.py $API_TOKEN /path/to/input.txt /path/to/output.csv')
    sys.exit()

try:
    sys.argv[4]
    print('I need 3 arguments')
    print('$ python3 bulk_domain_iqs.py $API_TOKEN /path/to/input.txt /path/to/output.csv')
    sys.exit()
except IndexError:
    # Do nothing
    nothing='nothing'

# Read input file
inputfilereader = open(inputfile, "r")

# Read output file (create if needed)
outputfilereader = open(outputfile, "w")
csvwriter = csv.writer(outputfilereader, delimiter=',')

urls = inputfilereader.read().split('\n')

print("###### Inicio ######")

# CSV headers
api_query='https://'+host+'/api/json/url/'+api_key+'/www.google.com'
response = requests.get(api_query)
data=json.loads(response.content)
data_flattened = flatten_json(data)
csvwriter.writerow(data_flattened.keys())

# CSV content
for i, url in enumerate(urls, start=1):
    print(f"Analyzing entry {i}/{len(urls)}")
    if url != '':
        api_query='https://'+host+'/api/json/url/'+api_key+'/'+url
        response = requests.get(api_query)
        if str(data.get('success','N/A')) == True:
            data=json.loads(response.content)
            data_flattened = flatten_json(data)
            csvwriter.writerow(data_flattened.values())
        else:
            print(str(data.get('message','N/A')))
            print("###### Terminado ######")
            sys.exit()

api_query='https://'+host+'/api/json/account/'+api_key
response = requests.get(api_query)
data=json.loads(response.content)
        
credit=str(data['credits'])

print("--> Remaining monthly queries: "+credit)
print("*** Daily lookup limit of 200 API hits not included")

print("###### Terminado ######")

