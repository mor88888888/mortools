# ------------------------------------------------------------------
# Create a csv report from AbuseIPDB about a list of IPs.
# 
# It requires an API token from AbuseIPDB.
#
# Author: mor8@protonmail.com
#
# Usage:
# $ python3 bulk_ips_abuseipdb.py $API_TOKEN /path/to/input.txt /path/to/output.csv
# -------------------------------------------------------------------

import sys
import getopt
import requests
import json

# Vars
inputfile = ''
outputfile = ''
inputfilereader = ''
outputfilereader = ''
ip_list = []
failed_list = []
last_ip = ''
api_key=''
host='api.abuseipdb.com'

# Check and save args
try:
    api_key = sys.argv[1]
    #print(api_key)
    inputfile = sys.argv[2]
    #print(inputfile)
    outputfile = sys.argv[3]
    #print(outputfile)
except IndexError:
    print('I need 3 arguments')
    print('check_ip_abuse.py API_KEY /path/to/input /path/to/output')
    sys.exit()

try:
    sys.argv[4]
    print('I need 3 arguments')
    print('check_ip_abuse.py API_KEY /path/to/input /path/to/output')
    sys.exit()
except IndexError:
    # Do nothing
    nothing='nothing'

# Read input file
inputfilereader = open(inputfile, "r")

# Read output file (create if needed)
outputfilereader = open(outputfile, "w")

ips = inputfilereader.read().split('\n')

print("###### Inicio ######")

# Cabecera del CSV
print('"IP","CC","ISP","Domain","Hostnames","Score","numReports","numusersrep","lastReportedAt","whitelist"')
outputfilereader.write('"IP","CC","ISP","Domain","Hostnames","Score","numReports","numusersrep","lastReportedAt","whitelist"\n')

for ip in ips:
    if ip != '':
        url='https://'+host+'/api/v2/check?ipAddress='+ip
        headers = {'Key': api_key}
        response = requests.get(url, headers=headers)
        data=json.loads(response.content)
        #vars
        cc=str(data['data']['countryCode'])
        isp=str(data['data']['isp'])
        domain=str(data['data']['domain'])
        hostnames=str(data['data']['hostnames'])
        score=str(data['data']['abuseConfidenceScore'])
        nrep=str(data['data']['totalReports'])
        nusers=str(data['data']['numDistinctUsers'])
        last_reported=str(data['data']['lastReportedAt'])
        whitelist=str(data['data']['isWhitelisted'])
        
        print('"'+ip+'","'+cc+'","'+isp+'","'+domain+'","'+hostnames+'",'+score+','+nrep+','+nusers+',"'+last_reported+'",'+whitelist)
        outputfilereader.write('"'+ip+'","'+cc+'","'+isp+'","'+domain+'","'+hostnames+'",'+score+','+nrep+','+nusers+',"'+last_reported+'",'+whitelist+'\n')

print("###### Terminado ######")

