# ------------------------------------------------------------------
# Checks, given a list of IPs, if any of them is in any blacklist
# and its reputation score.
# 
# It requires an API token from APIVOID.
#
# Author: mor8@protonmail.com
#
# Usage:
# $ python3 bulk_ip_apivoid.py $API_TOKEN /path/to/input.txt /path/to/output.csv
# -------------------------------------------------------------------

import sys
import getopt
import requests
import json
import time

# Vars
inputfile = ''
outputfile = ''
inputfilereader = ''
outputfilereader = ''
ip_list = []
failed_list = []
last_ip = ''
api_key=''
host='endpoint.apivoid.com'

# Check and save args
try:
    api_key = sys.argv[1]
    #print(api_key)
    inputfile = sys.argv[2]
    #print(inputfile)
    outputfile = sys.argv[3]
    #print(outputfile)
except IndexError:
    print('I need 3 arguments')
    print('check_ip_blacklist.py API_KEY /path/to/input /path/to/output')
    sys.exit()

try:
    sys.argv[4]
    print('I need 3 arguments')
    print('check_ip_blacklist.py API_KEY /path/to/input /path/to/output')
    sys.exit()
except IndexError:
    # Do nothing
    nothing='nothing'

# Read input file
inputfilereader = open(inputfile, "r")

# Read output file (create if needed)
outputfilereader = open(outputfile, "w")

ips = inputfilereader.read().split('\n')

print("###### Inicio ######")

# Cabecera del CSV
print('"IP","Detections","Total Engines","Detection rate","Risk Score')
outputfilereader.write('"IP","Detections","Total Engines","Detection rate","Risk Score"\n')

for ip in ips:
    if ip != '':
        url='https://'+host+'/iprep/v1/pay-as-you-go/?key='+api_key+'&ip='+ip
        response = requests.get(url)
        data=json.loads(response.content)
        
        numdetections=str(data['data']['report']['blacklists']['detections'])
        numengines=str(data['data']['report']['blacklists']['engines_count'])
        detection_rate=str(data['data']['report']['blacklists']['detection_rate'])
        risk_score=str(data['data']['report']['risk_score']['result'])
        
        print('"'+ip+'",'+numdetections+','+numengines+',"'+detection_rate+'",'+risk_score)
        outputfilereader.write('"'+ip+'",'+numdetections+','+numengines+',"'+detection_rate+'",'+risk_score+'\n')
        
        # https://docs.apivoid.com - "You need to limit requests per second to max 2-3 requests"
        time.sleep(0.5)

print("###### Terminado ######")

