import os
import re

domain = 'intranet.net'

extract_ips_cmd = 'nslookup '+ domain +' | grep -o -E "[0-9].*\.[0-9].*\.[0-9].*\.[0-9]+"'
extract_ips = os.popen(extract_ips_cmd).read().split('\n')

for ip in extract_ips:
    if ip != '':
        extract_dns_cmd = 'nslookup ' + ip
        extract_dns = os.popen(extract_dns_cmd).read()
        if "**" in extract_dns:
            extract_dns_cmd = 'nslookup ' + ip + " " + ip
            extract_dns = os.popen(extract_dns_cmd).read()
            if "**" in extract_dns:
                print("-- " + ip + " = No DNS lookup")
        elif "name = " in extract_dns:
            name = re.findall('=\s.*\..*\.', extract_dns)[0]
            print("-- " + ip + " " + str(name))

exit
