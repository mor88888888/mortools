import bulkRep
import argparse

# Parametros para la aplicación
parser = argparse.ArgumentParser(
    prog='bulkRep.py',
    description='A foo that bars',
    epilog="And that's how you'd foo a bar")
parser.add_argument('-i', '--inputfile')
parser.add_argument('-k', '--apikey')

# valores
args = parser.parse_args()

# Read input file
inputfilereader = open(args.inputfile, "r")

# Parse to array input file
inputs = inputfilereader.read().split('\n')

print("###### Inicio ######")

# -- IPInfo.io
#results = bulkRep.bulkIPInfo(args.apikey,inputs)

# -- AbuseIPDB
results = bulkRep.bulkIPAbuse(args.apikey,inputs)
for result in range(len(results)):
    print(",".join(results[result]))

# -- IP Quality Score
#results = bulkRep.bulkDomainIQS(args.apikey,inputs)
#print(results)
#for result in range(len(results)):
#    print(",".join(results[result])) # Not working: TypeError: sequence item 1: expected str instance, bool found

#result = bulkRep.creditIQS(args.apikey)
#print("\n".join(result))

print("####### Fin ########")
