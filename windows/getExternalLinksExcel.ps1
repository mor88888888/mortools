# Crea una instancia de Excel
$excel = New-Object -ComObject Excel.Application

# Abre el archivo de Excel
$workbook = $excel.Workbooks.Open("C:\Users\User\Desktop\Prueba 1 Original.xlsx")

# Obtiene las referencias externas
$externalLinks = $workbook.LinkSources([Microsoft.Office.Interop.Excel.XlLink.xlExcelLinks])

# Muestra las referencias externas encontradas
if ($externalLinks -ne $null) {
    foreach ($link in $externalLinks) {
        Write-Host "Referencia externa encontrada: $link"
    }
} else {
    Write-Host "No se encontraron referencias externas."
}

# Cierra el libro de Excel
$workbook.Close()

# Cierra la instancia de Excel
$excel.Quit()
