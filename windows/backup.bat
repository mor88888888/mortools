@echo off
rem -----------------------------------
rem Author: mor8@protonmail.com
rem Description:
rem	Script that copies all dirs named in %save*% vars in a zip file
rem Requires:
rem	C:\Program Files\7-Zip\7z.exe
rem -----------------------------------

rem Date
set year=%date:~10,4%
set month=%date:~4,2%
set day=%date:~7,2%
set hour=%time:~0,2%
set minute=%time:~3,2%
set second=%time:~6,2%

rem Default dirs
set backup=%year%%month%%day%-%hour%_%minute%_%second%.zip
set temp_path=%temp%\backup_%year%%month%%day%-%hour%_%minute%_%second%
echo Creating %temp_path%
mkdir %temp_path%

rem Where do you want the backup?
set /P input="Copia en E:\ (0) o en Desktop (1) "
if "%input%" == "1" (
	set dir_out=%userprofile%\Desktop\%backup%
)
if "%input%" == "0" (
	set dir_out=E:\%backup%
)

rem Folders to backup
mkdir %temp_path%\xampp
mkdir %temp_path%\xampp\htdocs
mkdir %temp_path%\Documents
copy %USERPROFILE%\xampp\htdocs %temp_path%\xampp\htdocs
copy %USERPROFILE%\Documents %temp_path%\Documents

rem Compressing folders
echo Compressing into %temp_path%
"C:\Program Files\7-Zip\7z.exe" a -tzip "%dir_out%" "%temp_path%\*" -r

rem Eliminamos las marcas de backup
del %userprofile%\last_backup_*

rem Dejamos constancia de la fecha del backup
echo "Backup -- Directorio de salida: %dir_out%" > %userprofile%\last_backup_%year%%month%%day%-%hour%_%minute%_%second%.txt

rem Eliminamos el archivo temporal
del "%temp_path%"

rem Esperamos confirmación del usuario
pause
exit
