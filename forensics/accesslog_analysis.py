# ------------------------------------------------------------------
# Analyze Apache access_log from csv converted with accesslog2csv.pl
# Ref: https://gist.github.com/sepehr/fff4d777509fa7834531
#
# Author: mor8@protonmail.com
#
# Usage:
# $ python3 accesslog_analysis.py /path/to/input.csv /path/to/output/
# -------------------------------------------------------------------

import sys
import getopt
import numpy as np
import pandas as pd
import time
import csv

# Vars
inputfile = ''
outputpath = ''
inputfilereader = ''
outputfilereader = ''

# Check and save args
try:
    inputfile = sys.argv[1]
    #print(inputfile)
    outputpath = sys.argv[2]
    #print(outputpath)
except IndexError:
    print('I need 2 arguments')
    print('python3 accesslog_analysis.py /path/to/input.csv /path/to/output/')
    sys.exit()

try:
    sys.argv[3]
    print('I need 2 arguments')
    print('python3 accesslog_analysis.py /path/to/input.csv /path/to/output/')
    sys.exit()
except IndexError:
    # Do nothing
    nothing='nothing'

# Read input file
print("INFO: Reading input file")
dataset = pd.read_csv(inputfile)

# Bytes ops
print("INFO: Bytes stats")
summary_bytes = dataset['Bytes Sent'].describe()
outputfilereader = open(outputpath+"/bytes.txt", "w")
outputfilereader.write(str(summary_bytes)+"\n")

# Status code ops
print("INFO: Status stats")
code_mode = dataset['Response Code'].mode()
outputfilereader = open(outputpath+"/status.txt", "w")
outputfilereader.write("Código de estado con más resultados: "+str(code_mode)+"\n")

# Host ops
print("INFO: Host stats")
summary_hosts = dataset['Host'].describe()
host_mode = dataset['Host'].mode()
outputfilereader = open(outputpath+"/host.txt", "w")
outputfilereader.write("Origen con más resultados: "+str(host_mode)+"\n")

# Output
print("-- File loaded first rows --")
print(dataset.head())
