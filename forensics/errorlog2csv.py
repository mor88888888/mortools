# ------------------------------------------------------------------
# Convert error.log file (Apache HTTP Server) to a CSV file
#
# Author: mor8@protonmail.com
#
# Usage:
# $ python3 errorlog2csv.py /path/to/input.log /path/to/output.csv
# -------------------------------------------------------------------

import csv
import sys
import re

def mostrar_ayuda():
    print("Uso: python apache_log_converter.py archivo_entrada archivo_salida")
    print("Convierte un archivo error.log de Apache a formato CSV.")
    print("Argumentos:")
    print("  archivo_entrada: Nombre del archivo error.log de Apache.")
    print("  archivo_salida: Nombre deseado para el archivo CSV de salida.")
    print("Ejemplo: python apache_log_converter.py error.log error_log.csv")

def extraer_ip_cliente(mensaje):
    patron_ip = r"\[client ([^\]]+)\]"
    coincidencia = re.search(patron_ip, mensaje)
    if coincidencia:
        return coincidencia.group(1)
    else:
        return ""

def convertir_error_log_a_csv(archivo_entrada, archivo_salida):
    patron = r'\[(.*?)\] \[(.*?)\] \[pid (.*?)\] (.*)'
    with open(archivo_entrada, 'r') as archivo_entrada, open(archivo_salida, 'w', newline='') as archivo_salida:
        escritor_csv = csv.writer(archivo_salida)
        escritor_csv.writerow(['Fecha', 'Nivel', 'PID', 'IP', 'Mensaje'])

        for linea in archivo_entrada:
            # Ejemplo de formato de registro de error de Apache:
            # [Fri May 26 08:15:58.550360 2023] [fcgid:warn] [pid 15297:tid 140697793378048] (32)Broken pipe: [client 84.76.207.82:44694] mod_fcgid: ap_pass_brigade failed in handle_request_ipc function
            coincidencia = re.match(patron, linea)
            if coincidencia:
                fecha = coincidencia.group(1)
                nivel = coincidencia.group(2)
                pid = coincidencia.group(3)
                mensaje = coincidencia.group(4)
                ip_cliente = extraer_ip_cliente(mensaje)
                mensaje = re.sub(r"\[client [^\]]+\]", "", mensaje).strip()  # Eliminar la IP del mensaje
                escritor_csv.writerow([fecha, nivel, pid, ip_cliente, mensaje])

    print(f"Archivo CSV generado con éxito: {archivo_salida}")

# Comprobar si se proporciona la opción de ayuda
if len(sys.argv) > 1 and (sys.argv[1] == '--help' or sys.argv[1] == '-h'):
    mostrar_ayuda()
elif len(sys.argv) == 3:
    archivo_error_log = sys.argv[1]
    archivo_csv_salida = sys.argv[2]
    convertir_error_log_a_csv(archivo_error_log, archivo_csv_salida)
else:
    print("Argumentos inválidos. Utiliza '--help' para obtener ayuda.")

