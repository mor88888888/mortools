#!/bin/bash

# Get the number of entries per month given a timeline of an MFT.

condition=$1

echo "year;month;results"

for y in {2020..2022}
do
	for m in {1..12}
	do
		if [[ $m -gt 9 ]]
		then
			result=$(grep -i $condition timeline.csv | grep -E "^$y-$m" | wc -l)
		else
			result=$(grep -i $condition timeline.csv | grep -E "^$y-0$m" | wc -l)
		fi
		
		if [[ $result -gt 0 ]]
		then
			echo "$y;$m;$result"
		fi
	done
done
