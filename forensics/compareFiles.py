import sys, getopt
import socket
import argparse

# Vars
inputfilereader1 = ''
inputfilereader2 = ''
outputfilereader = ''
result = []

# Parametros para la aplicación
parser = argparse.ArgumentParser(
    prog='compareFiles.py',
    description='A foo that bars',
    epilog="And that's how you'd foo a bar")
parser.add_argument('-1', '--file1')
parser.add_argument('-2', '--file2')
parser.add_argument('-c', '--coincidence', action=argparse.BooleanOptionalAction)
parser.add_argument('-d', '--difference', action=argparse.BooleanOptionalAction)
parser.add_argument('-o', '--output')

# valores
args = parser.parse_args()

# Read input file
inputfilereader1 = open(args.file1, "r")
inputfilereader2 = open(args.file2, "r")

# Read output file (create if needed)
outputfilereader = open(args.output, "w")

lines1 = inputfilereader1.read().split('\n')
lines2 = inputfilereader2.read().split('\n')

if args.coincidence:
    for line2 in lines2:
        if line2 in lines1 and line2 != '':
                result.append(line2)

if args.difference and not args.coincidence:
    for line2 in lines2:
        if line2 not in lines1 and line2 != '':
                result.append(line2)
    for line1 in lines1:
        if line1 not in lines2 and line1 != '':
                result.append(line1)

print(list(set(result)))