#!/bin/bash

# Check how many results are per day.

if ! command -v rg &>/dev/null; then
	echo "The tool 'rg' (ripgrep) is not installed"
	exit 1
fi

y=2023

for m in {1..12}
do
	for d in {1..31}
	do
		# $d and $m has to be in a 2 digit format
		if [ ${#m} -eq 1 ]; then
			m="0$m"
		fi

		if [ ${#d} -eq 1 ]; then
			d="0$d"
		fi
		
		# Execute the filter
		result=$(rg "^$y-$m-$d" $1 | wc -l)
		
		if [[ $result -gt 0 ]]
		then
			# Print result
			echo "$y-$m-$d,$result"
		fi
	done
done
