#!/bin/bash

# Check how many results are per month.

if ! command -v rg &>/dev/null; then
	echo "The tool 'rg' (ripgrep) is not installed"
	exit 1
fi

for y in {2020..2023}
do
	for m in {1..12}
	do
		# $d and $m has to be in a 2 digit format
		if [ ${#m} -eq 1 ]; then
			m="0$m"
		fi
		
		# Execute the filter
		result=$(rg "^$y-$m" $1 | wc -l)
		
		if [[ $result -gt 0 ]]
		then
			echo "$y-$m,$result"
		fi
	done
done
