# MFTExtractor
Powerhsell script that lists all mounted drives and extracts the MFT table in `./output/`.

## Prepare
```bash
mkdir mftextractor
cd mftextractor
wget https://gitlab.com/mor88888888/mortools/-/raw/main/forensics/mftextractor/run.ps1
mkdir bin
mkdir output
wget https://github.com/jschicht/RawCopy/raw/master/RawCopy64.exe -o bin/rawcopy.exe
cd ..
zip -r MFTExtractor.zip mftextractor/
```

## Usage
```powershell
Expand-Archive -LiteralPath MFTExtractor.zip -DestinationPath .
cd mftextractor
.\run.ps1
```

## Requirements
- Requires "/bin/rawcopy.exe" from https://github.com/jschicht/RawCopy
- Requires local administrator permissions.
