# Obtener la ruta del directorio actual
$currentDirectory = Split-Path -Parent $MyInvocation.MyCommand.Path

# Ruta de la herramienta rawcopy
$rawcopyPath = Join-Path -Path $currentDirectory -ChildPath "\bin\rawcopy.exe"

# Ruta de salida para los archivos RAW de la tabla MFT (por defecto: directorio actual)
$outputPath = $currentDirectory+"\output\"

# Comprobar la existencia y validez de la ruta de rawcopy.exe
if (-not (Test-Path $rawcopyPath) -or (Split-Path -Parent $rawcopyPath) -eq '') {
    Write-Host "La ruta de la herramienta rawcopy no es válida o no existe."
    Exit
}

# Comprobar la existencia y validez de la ruta de salida
if (-not (Test-Path $outputPath) -or (Split-Path -Parent $outputPath) -eq '') {
    Write-Host "La ruta de salida no es válida o no existe."
    Exit
}

# Obtener las unidades montadas en el sistema
$drives = Get-WmiObject Win32_Volume | Where-Object { $_.DriveLetter -ne $null }

# Recorrer cada unidad y extraer la tabla $MFT en formato RAW
foreach ($drive in $drives) {
    $driveLetter = $drive.DriveLetter

    # Verificar que la unidad sea una unidad local
    if ($drive.DriveType -eq "3") {
        Write-Host "Extrayendo tabla MFT en formato RAW de la unidad $driveLetter"

        # Construir la ruta completa del archivo de salida
        #$outputFile = "$($driveLetter.TrimEnd(':'))-MFT.raw"
		$outputFile = "$env:COMPUTERNAME-$($driveLetter.TrimEnd(':'))-MFT.raw"
		
		Write-Host $outputFile
        # Ejecutar rawcopy para extraer la tabla $MFT en formato RAW
		& $rawcopyPath /FileNamePath:$driveLetter"0" /OutputPath:$outputPath /OutputName:$outputFile

    }
}
