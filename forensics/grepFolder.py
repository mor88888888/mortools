import sys
import csv
from collections import Counter

depth = 7  # Cambiar esto al nivel de profundidad deseado
path_row = 7 # Suponiendo que la ocatava columna (índice 7) contiene la ruta de la carpeta
padding = 3 # Si la ruta comienza con carpetas que no son del SO recuperado, si no donde se ha montado para el análisis
timeline_file = sys.argv[1] # Ruta de la timeline a analizar
target_folders = []
exceptions = ["Windows","$Extend","$OrphanFiles", "Program Files", "Program Files (x86)"]

# Abrir el archivo CSV
with open(timeline_file, 'r', newline='') as csvfile:
	csvreader = csv.reader(csvfile)
	
	# Iterar a través de las filas del archivo CSV
	for row in csvreader:
		
        # Extrae el path
		folder_path = row[path_row]
		
		# Separar la ruta de la carpeta en un array
		folder_levels = folder_path.strip('/').split('/')
		
        # Lee la ruta hasta la profundida deseada
		target_folder = folder_levels[:depth]

		# Delete firts rows if needed
		for i in range(0,padding):
			if len(target_folder) > 2:
				target_folder.pop(0)
		
		target_folders.append("/".join(target_folder))

	# Objeto "Counter" para sacar estadísticas
	contador = Counter(target_folders)

	# Imprimir el resultado en csv
	print("Carpeta,Operaciones")
	for elemento, cantidad in contador.items():
		folders_name = elemento.split('/')
		res = [ele for ele in exceptions if(ele in folders_name)]
		hide = bool(res)
		if cantidad > 10 and not hide:
			print(f"{elemento},{cantidad}")
